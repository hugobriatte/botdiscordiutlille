grep = lambda word, chain : chain.find(word) != -1

cut = lambda chain, separator, index : chain.split(separator)[min(index,chain.count(separator))]

def cutC(chain, listIndex):
	res=""
	for idx in listIndex:
		res=res+chain[idx]
	return res

def tr(chain, char1, char2):
	result = ''
	for c in chain:
		if c == char1:
			result += char2
		else:
			result += c
	return result
