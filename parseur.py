from date import Date
from cour import Cour
from enum import Enum
import os
from shellCommand import *

class Parseur:
	@staticmethod
	def readIcs(fichier):
		text = os.popen("cat " + fichier + " | tr '\n' '|'").read()
		listElems = text.split("|")
		listTrim = []
		for element in listElems:
			listTrim.append(tr(element,"\\","").strip())

		# Beginning analyse
		ensCours = set()

		dateDebut = None
		dateFin = None
		matiere = None
		prof = None
		groupes = None
		salle = None
		ds = False
		absent = False

		flag_NewEvent = False
		for element in listTrim:
			if grep('BEGIN:VEVENT', element):
				flag_NewEvent = True
			if grep('END:VEVENT', element):
				flag_NewEvent = False
				#Technique de flemmard pour enelever les entrées aux dates incorrectes lol (les incorporer a la prochaine update)
				if not grep("Férié", matiere) and not grep("Vacances", matiere):
					test = Cour(dateDebut, dateFin, matiere, prof, groupes, salle, ds, absent)
					ensCours.add(test)
				dateDebut = None
				dateFin = None
				matiere = None
				prof = None
				groupes = None
				salle = None
				ds = False
				absent = False
			if flag_NewEvent == True:
				if grep('DTSTART:', element):
					anneeMoisJourHeureMin = cut(element,':',1)
					annee = cutC(anneeMoisJourHeureMin,[0,1,2,3])
					mois = cutC(anneeMoisJourHeureMin,[4,5])
					jour = cutC(anneeMoisJourHeureMin,[6,7])
					heure = int(cutC(anneeMoisJourHeureMin,[9,10]).strip()) + 2
					minute = int(cutC(anneeMoisJourHeureMin,[11,12]).strip())
					dateDebut = Date(annee, mois, jour, heure, minute)
				elif grep('DTEND:', element):
					anneeMoisJourHeureMin=cut(element,':',1)
					annee = cutC(anneeMoisJourHeureMin,[0,1,2,3])
					mois = cutC(anneeMoisJourHeureMin,[4,5])
					jour = cutC(anneeMoisJourHeureMin,[6,7])
					heure = int(cutC(anneeMoisJourHeureMin,[9,10]).strip()) + 2
					minute = int(cutC(anneeMoisJourHeureMin,[11,12]).strip())
					dateFin = Date(annee, mois, jour, heure, minute)
				elif grep('SUMMARY', cut(element, ';', 0)):
					if(cut(element,":",1).find("absent")!=-1 or cut(element,":",1).find("disponible")!=-1):
						matiere = cut(cut(element,":",2)," - ",0)
						absent = True
						prof = cut(cut(element,":",2)," - ",1)
					else:
						matiere = cut(cut(element,":",1)," - ",0)
						prof = cut(cut(element,":",2)," - ",1)
					groupes = ""
					lettres = "GHIJKL"
					for lettre in lettres:
						if grep("Groupe "+lettre,cut(element," - ",2)):
							groupes = groupes + lettre
				elif grep('LOCATION', element):
					salle = cut(element,":",1).strip()
					if salle == "DS":
						ds = True
		return ensCours
