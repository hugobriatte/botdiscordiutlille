from date import Date
from cour import Cour
import os
def downloadAll(listLien):
	listLettre="GHIJKL"
	i=0
	while i<len(listLien):
		os.popen("wget --quiet \""+listLien[i]+"\" ; mv $(basename \""+listLien[i]+"\") Groupe"+listLettre[i]+".ics")
		i=i+1
def triDateDeb(listCours,date):
	res=[]
	for cour in listCours:
		if cour.dateDeb==date:
			res.append(cour)
	return res
def triDateFin(listCours,date):
	res=[]
	for cour in listCours:
		if cour.dateFin==date:
			res.append(cour)
	return res
def triMatiere(listCours,matiere):
	res=[]
	for cour in listCours:
		if cour.matiere==matiere:
			res.append(cour)
	return res
def triSalle(listCours,salle):
	res=[]
	for cour in listCours:
		if cour.salle==salle:
			res.append(cour)
	return res
def triGroupe(listCours,groupes):
	res=[]
	for cour in listCours:
		g=0
		while g<len(groupes) and cour.groupes.find(groupes[g])!=-1:
			g=g+1
		if g==len(groupes):
			res.append(cour)
	return res
def triDs(listCours,ds):
	res=[]
	for cour in listCours:
		if cour.ds==ds:
			res.append(cour)
	return res
def triEntreDate(listCours,dateA,dateB):
	res=[]
	for cour in listCours:
		if cour.date>=dateA and cour.date<=dateB:
			res.append(cour)
	return res
def triAvantDate(listCours,date):
	res=[]
	for cour in listCours:
		if cour.date<dateA:
			res.append(cour)
	return res
def triApresDate(listCours,date):
	res=[]
	for cour in listCours:
		if cour.date>dateA:
			res.append(cour)
	return res
downloadAll(["http://iutinfohyp.univ-lille1.fr:81/hp/Telechargements/ical/Edt_LEZORAINE.ics?version=2018.0.3.3&idICal=2A6EA5757ED1DEA9B4CD147A55F99716&param=643d5b312e2e36325d2666683d3126663d31","http://iutinfohyp.univ-lille1.fr:81/hp/Telechargements/ical/Edt_BRIATTE.ics?version=2018.0.3.3&idICal=176827FA22A9546A8E04AA46CC9F6DEF&param=643d5b312e2e36325d2666683d3126663d31"])
cour1=Cour(Date(2019,1,20,13,39),Date(2019,1,20,14,0),"Francais","GHI","A035",True)
cour2=Cour(Date(2019,1,20,14,00),Date(2019,1,20,14,30),"Maths","HIJK","A036",True)
cour3=Cour(Date(2019,1,20,14,30),Date(2019,1,20,15,00),"Maths","IKJ","A036",True)
critereGroupe="G"
print("List des cours qui prennent les groupes : "+critereGroupe+"\n"+str(triGroupe([cour1,cour2,cour3],critereGroupe)))
