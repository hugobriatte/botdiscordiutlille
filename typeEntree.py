from enum import Enum
from date import Date
from traitementString import TraitementString
import os
import re
class TypeEntree(Enum):
	DATE = 0
	GROUPE = 1
	MP = 2
	UE = 3

	#TypeEntreeAttendu : JJ/MM
	def traitementDate(self,entree):
		ajd = Date.getToday(keepHeureMin = False)
		if len(entree)==0 or entree=="ajd":
			return ajd
		elif entree=="demain":
			return ajd.jourProchain()
		elif not re.search(r'^\d+/\d+$',entree):
			raise TypeError
		else:
			try:
				entree = entree.split("/")
				jour = int(entree[0])
				mois = int(entree[1])
				return Date(2019,mois,jour,0,0)
			except ValueError as e:
				return e

	#TypeEntreeAttendu : Une lettre pour le groupe
	def traitementGroupe(self,author,entree):
		LETTRES = "GHIJKLT"
		if len(entree)==0:
			for role in author.roles:
				nomRole=role.name
				if nomRole.find("Groupe")!=-1:
					for lettre in LETTRES:
						if nomRole.rfind(lettre)==7:
							return lettre
			raise ValueError("Role non trouvé")
		elif len(entree)==1:
			if LETTRES.find(entree.upper()) == -1:
				raise ValueError("Lettre entrée incorrect") 
			return entree
		else:
			raise TypeError

	#TypeEntreeAttendu : UE1 ou UE2
	def traitementUE(self,arg):
		arg = arg.upper()
		if (arg == ""):
			return 0
		elif (arg == "UE2"):
			return 2
		elif (arg == "UE1"):
			return 1
		elif (re.search(r'^UE\d+$')):
			raise ValueError("Numero d'UE incorrect")
		else:
			raise TypeError

	#TypeEntreeAttendu : MP
	def traitementMP(self,arg):
		arg = arg.upper()
		if (arg == ""):
			return False
		elif (arg == "MP"):
			return True
		else:
			raise TypeError

	#Detecte le type d'argument d'un argument
	@staticmethod
	def detectTypeArg(auteur,arg,ensTypeArgs):
		for typeArg in ensTypeArgs:
			try:
				return [typeArg,typeArg.verif(auteur,arg)]
			except ValueError as e:
				return [typeArg,e]
			except Exception as e:
				pass
		return [None,TypeError]

	#Verifie si un message contient des arguments correct selon une ensemble de type d'argument
	#Renvoie un dictionnaire avec pour clé les types d'arguments et pour valeurs l'argument associé et sa représentation en objet selon le type d'argument
	@staticmethod
	def controleMessage(message,ensTypeArgs):
		ensArgs = TraitementString.ensOfArgs(message.content)
		auteur = message.author
		if len(ensArgs) > len(ensTypeArgs):
			raise ValueError("Trop d'argument inseré! " + str(len(ensArgs)) + " trouvé. " + str(len(ensTypeArgs)) + " argument(s) revevable au maximum")
		else:
			dictTypeArg = {}
			messageErreurType = "Un(des) argument(s) a(ont) été mal interprété : "
			for arg in ensArgs:
				typeArgRep = TypeEntree.detectTypeArg(auteur,arg,ensTypeArgs)
				dictTypeArg[typeArgRep[0]] = [arg,typeArgRep[1]]
				if type(typeArgRep[1]) == ValueError:
					raise ValueError("L'argument " + arg + " a été interprété comme un " + str(typeArgRep[0]) + " , mais celui-ci a été mal renseigné : " + str(typeArgRep[1]))
				elif typeArgRep[1] == TypeError:
					messageErreurType += str(arg) + ", "
				else:
					ensTypeArgs.remove(typeArgRep[0])
			if messageErreurType != "Un(des) argument(s) a(ont) été mal interprété : ":
				raise TypeError(messageErreurType)
			else:
				ensTypeArgsTraite = set()
				for typeArg in ensTypeArgs:
					if typeArg not in dictTypeArg.keys() and typeArg not in ensTypeArgsTraite:
						dictTypeArg[typeArg] = ["",typeArg.verif(auteur,"")]
						if type(dictTypeArg[typeArg][1]) == ValueError:
							raise ValueError("L'argument 'Chaine Vide' a été interprété comme un " + str(typeArg) + " , mais celui-ci a été mal renseigné : " + str(dictTypeArg[typeArg][1]))
						else:
							ensTypeArgsTraite.add(typeArg)
				return dictTypeArg

	#Verifie si un argument correspond au Type D'argument de l'objet
	def verif(self,auteur,arg):
		if self.value == 0 :
			try:
				return self.traitementDate(arg)			
			except Exception as e:
				raise e
		elif self.value == 1 :
			try:
				return self.traitementGroupe(auteur,arg)
			except Exception as e:
				raise e
		elif self.value == 2 :
			try:
				return self.traitementMP(arg)
			except Exception as e:
				raise e
		else :
			try:
				return self.traitementUE(arg)
			except Exception as e:
				raise e
