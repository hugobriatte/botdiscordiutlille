# coding: utf-8
from date import Date
from shellCommand import *
class Cour:
	nbrCour = 0
	@classmethod
	def initFromNumber(cls,*arg,dateDeb = {'annee' : 0, 'mois' : 1,'jour' : 1, 'heure' : 0, 'minute' : 0},
			   dateFin = {'annee' : 0, 'mois' : 1,'jour' : 1, 'heure' : 0, 'minute' : 0},
			   matiere = "Inconnu", prof = "Inconnu", groupes = "Inconnu", salle = "Inconnu",
			   ds = False, abscent = False):
		print("lol")
	def __init__(self,dateDeb,dateFin,matiere,prof,groupes,salle,ds,abscent):
		if isinstance(dateDeb,Date):
			self.dateDeb=dateDeb
		else:
			self.dateDeb=Date(0,0,0,0,0)
		if isinstance(dateFin,Date):
			self.dateFin=dateFin
		else:
			self.dateFin=Date(0,0,0,0,0)
		self.matiere=str(matiere)
		self.prof=str(prof)
		if len(groupes) != 0:
			self.groupes=str(groupes)
		else:
			self.groupes="T"
		self.salle=str(salle)
		self.ds=bool(ds)
		self.absent=bool(abscent)
		self.idCour = Cour.nbrCour
		Cour.nbrCour += 1
	def stringClair(self):
		msg = ""
		if self.ds or self.estCTP():
			msg += "**"
		if self.absent:
			msg += "~~"
		if self.dateDeb.minute == 0:
			msg += "De " + str(self.dateDeb.heure) + "h"
		else:
			msg += "De " + str(self.dateDeb.heure) + "h" + str(self.dateDeb.minute)
		if self.dateFin.minute == 0:
			msg += " à " + str(self.dateFin.heure) + "h"
		else:
			msg += " à " + str(self.dateFin.heure) + "h" + str(self.dateFin.minute)
		msg += " : "
		ded = ""
		if "AEIOUYaeiouy".find(self.matiere[0]) == -1:
			ded = "de "
		else:
			ded = "d'"
		if self.ds:
			msg += "DS " + ded + self.matiere
		elif self.estCTP():
			msg += "CTP " + ded + self.matiere
		else:
			if self.estAmphi():
				msg += "Amphi " + ded
			else:
				msg += "Cour " + ded
			msg += self.matiere + " avec " + self.prof + " en "
			if self.salle.find(", INFO") != -1:
				msg += cut(self.salle,",",0)
			else:
				msg += self.salle
			if len(self.groupes)!=1:
				msg += " avec les groupes "
				for lettre in self.groupes:
					msg+=lettre+","
				msg=msg[:-1]
		if self.ds or self.estCTP():
			msg += "**"
		if self.absent:
			msg += "~~ **(PROF ABSENT OU NON DISPONIBLE)**"
		return msg
	def toutLesGroupes(self):
		return self.groupes=="GHIJKL"
	def estCTP(self):
		return self.toutLesGroupes() and self.salle.find("INFO") != -1 and self.salle.find("Grands Amphis") == -1
	def estAmphi(self):
		return self.toutLesGroupes() and (self.salle.find("1")==0 or self.salle.find("Grands Amphis") != -1)
	def __eq__(self,autre):
		if isinstance(autre,self.__class__):
			return self.dateDeb==autre.dateDeb and self.dateFin==autre.dateFin and self.matiere==autre.matiere and self.groupes==autre.groupes and self.salle==autre.salle and self.ds==autre.ds and self.absent==autre.absent
		return False

	def __ne__(self,autre):
		if isinstance(autre,self.__class__):
			return self.dateDeb!=autre.dateDeb or self.dateFin!=autre.dateFin or self.matiere!=autre.matiere or self.groupes!=autre.groupes or self.salle!=autre.salle or self.ds!=autre.ds or self.absent!=autre.absent
		return True

	def __gt__(self,autre):
		return self.dateDeb>autre.dateDeb

	def __ge__(self,autre):
		return self.dateDeb>=autre.dateDeb

	def __lt__(self,autre):
		return self.dateDeb<autre.dateDeb

	def __le__(self,autre):
		return self.dateDeb<=autre.dateDeb

	def __repr__(self):
		affichage = "Date debut:\t\t" + str(self.dateDeb) + "\nDate fin:  \t\t" + str(self.dateFin) + "\nMatiere:   \t\t" + self.matiere + "\nProf:      \t\t" + self.prof
		if len(self.groupes)!=0:
			affichage = affichage + "\nGroupe(s): \t\t" + self.groupes
		else:
			affichage = affichage + "\nGroupe(s): \t\tDU Tremplin"
		affichage = affichage + "\nSalle:     \t\t" + self.salle
		if self.ds == True:
			affichage = "\t/!\\ DS /!\\\n-------------------------------------------------------------\n" + affichage
		if self.absent == True:
			affichage = "\t/!\\ Enseignant absent /!\\\n-------------------------------------------------------------\n" + affichage
		return affichage
	def __hash__(self):
		return hash(self.idCour)
