class TraitementString:
	@staticmethod
	def ensOfArgs(message):
		res = set()
		arg = ""
		premierEspace = True
		for lettre in message:
			if not lettre.isspace():
				arg += lettre
			elif premierEspace:
				arg = ""
				premierEspace = False
			elif len(arg) != 0:
				res.add(arg)
				arg = ""
		if not premierEspace:
			res.add(arg)
		return res
