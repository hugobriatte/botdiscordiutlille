# coding: utf8
from date import Date
from cour import Cour
from parseur import Parseur
from typeEntree import TypeEntree
import os
import discord
import wget
from shellCommand import *
TOKEN = 'Insere ici le token de ton bot discord'

client = discord.Client()

LETTRES="GHIJKLT"

def downloadAll(listLien):
	for i, lettre in enumerate(LETTRES):
		nomAttendu = "Groupe" + lettre + ".ics"
		fichier=wget.download(url=listLien[i],out="Groupe"+lettre+".ics")
		os.popen("mv -f '"+fichier+"' '"+nomAttendu+"'")

listCours=[]	

def getAllCour():
	downloadAll(["Insere ici", "Le emplois du temps", "Dans l'ordre", "Et sans te tauler", "Voili", "Voilou"])
	listCours.clear()
	for lettre in LETTRES:
		listCours.append(Parseur.readIcs("Groupe"+lettre+".ics"))

def get_ens_cours(t,g):
	lendemain = t.jourProchain(keepHeureMin = False) 
	result = set()
	for cour in listCours[LETTRES.find(g.upper())]:
		if cour.dateDeb >= t and cour.dateDeb <= lendemain:
			result.add(cour)
	return result

def getNextCour(g):
	dateProchainCour = Date.getToday(keepHeureMin = True)
	dateProchainCour.removeMinute(15)
	for cour in sorted(get_ens_cours(dateProchainCour,g)):
		if cour.dateDeb >= dateProchainCour:
			return cour.stringClair()
	return "La journée est fini !"

def affNext(message):
	msg = ""
	mp = False
	try:
		dictTypeArg = TypeEntree.controleMessage(message,{TypeEntree.GROUPE,TypeEntree.MP})
		groupe = dictTypeArg[TypeEntree.GROUPE][1]
		mp = dictTypeArg[TypeEntree.MP][1]
		msg = "**__Prochain cour du groupe " + groupe + " : __**" + getNextCour(groupe)
	except ValueError as e:
		msg = str(e)
	except TypeError as e:
		msg = str(e)
	if mp:
		return msg, message.author
	else:
		return msg, message.channel

def affEdt(message):
	msg = ""
	mp = False
	try:
		dictTypeArg = TypeEntree.controleMessage(message,{TypeEntree.DATE,TypeEntree.GROUPE,TypeEntree.MP})
		date = dictTypeArg[TypeEntree.DATE][1]
		groupe = dictTypeArg[TypeEntree.GROUPE][1]
		mp = dictTypeArg[TypeEntree.MP][1]
		msg = '**__Emploi du temps du groupe ' + groupe + ' pour le ' + cut(os.popen("date -d "+cut(str(date.mois)+"/"+str(date.jour)+"/"+str(date.annee),' ',0)).read(),',',0) + ' : __**\n\n'
		for cour in sorted(get_ens_cours(date,groupe)):
			msg += cour.stringClair()+"\n"
	except ValueError as e:
		msg = str(e)
	except TypeError as e:
		msg = str(e)
	if mp:
		return msg, message.author
	else:
		return msg, message.channel
@client.event
async def on_message(message):
	if message.author == client.user:
		return
	if message.channel.name == "emploi-du-temps":
		if message.content.startswith('!prochain'):
			contenu, dest = affNext(message)
			if dest != message.channel:
				await client.send_message(message.channel, "Reponse envoyé en privée".format(message))
			await client.send_message(dest, contenu.format(message))
                if message.content.startswith('!edt'):
			contenu, dest = affEdt(message)
			if dest != message.channel:
				await client.send_message(message.channel, "Reponse envoyé en privée".format(message))
			await client.send_message(dest, contenu.format(message))
		if message.content.startswith('!update'):
			await client.send_message(message.channel, "Je met a jour mes données, attendez 10 secondes puis lancez votre requête".format(message))
			getAllCour()
			await client.send_message(message.channel, "Wala".format(message))
			if message.content.startswith('!aide'):
				await client.send_message(message.channel, "Voici les differentes commandes que je peux gérer:\n`!edt` : Envoie l'emploi du temps de votre groupe pour aujourd'hui. Vous pouvez modifier la requête en rajoutant a la suite:\n\tLa lettre de votre groupe (g,h,i,j,k,l ou t pour les tremplins) Exemple: `!edt G` renvoie l'emploi du temps du groupe G pour aujourd'hui\n\tLa date de l'emploi du temps du groupe (demain ou JJ/MM) Exemple: `!edt demain` renvoie l'emploi du temps de votre groupe pour demain ; `!edt 25/2` renvoie l'emploi du temps de votre groupe pour le 25 février\n\tEt/Ou le mot clé `mp` pour recevoir votre réponse en privée\n`!prochain` : Envoie le prochain cour de votre groupe pour aujord'hui. Il peut être précédé de la lettre d'un groupe ou bien du mot clé mp lui aussi\n`!update` : Actualise tout les emplois du temps".format(message))
	else:
		return
@client.event
async def on_ready():
	await client.change_presence(game=discord.Game(name="!aide"))
getAllCour()
client.run(TOKEN)
