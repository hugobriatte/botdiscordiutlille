import os
class Date:
	"""Cette classe definit une Date avec année, mois, jour, heure et minute"""
	def __init__(self,
		     annee,mois,jour,
		     heure,minute):
		self.annee = int(annee)
		self.mois = int(mois)
		self.jour = int(jour)
		self.heure = int(heure)
		self.minute = int(minute)
	@property
	def annee(self):
		"""Année de la date"""
		return self.__annee

	@annee.setter
	def annee(self, arg):
		assert arg >= 0, "L'année donné est incorrect"
		self.__annee = arg

	@property
	def mois(self):
		"""Mois de la date"""
		return self.__mois

	@mois.setter
	def mois(self, arg):
		assert arg >= 1 and arg <= 12, "Le mois donné est incorrect"
		self.__mois = arg

	@property
	def jour(self):
		"""Jour de la date"""
		return self.__jour

	@jour.setter
	def jour(self, arg):
		assert Date.estNbJourValide(self.annee,self.mois,arg), "Le jour donné est incorrect"
		self.__jour = arg

	@property
	def heure(self):
		"""Heure de la date"""
		return self.__heure

	@heure.setter
	def heure(self, arg):
		assert arg >= 0 and arg < 24, "L'heure donnée est incorrect"
		self.__heure = arg

	@property
	def minute(self):
		"""Minute de la date"""
		return self.__minute

	@minute.setter
	def minute(self, arg):
		assert arg >= 0 and arg < 60, "La minute donné est incorrect"
		self.__minute = arg
	
	@staticmethod
	def estAnneeBissextile(annee):
		"""Retourne vrai si l'année donnée est bissextile, faux sinon"""
		return (annee % 400 == 0
			or (annee % 4 == 0 and annee % 100 != 0))

	@staticmethod
	def getToday(keepHeureMin = False):
		"""Retourne la date d'aujord'hui
		Si keepHeureMin est False, l'heure et la minute sont defini a 0, sinon elles sont defini a l'heure et la minute de maitenant
		"""
		donneeTxt = os.popen("date +%F-%H-%M").read().split('-')
		if keepHeureMin:
			return Date(donneeTxt[0],donneeTxt[1],
				    donneeTxt[2],donneeTxt[3],donneeTxt[4])
		else:
			return Date(donneeTxt[0],donneeTxt[1],
				    donneeTxt[2],0,0)

	def addHeure(self,heure):
		"""Ajoute un nombre d'heure donnée a une date

		>>> d = Date(2019,12,24,20,5)
		>>> print(d)
		24/12/2019 20:05
		>>> d.addHeure(10)
		>>> print(d)
		25/12/2019 06:05
		"""
		while heure + self.heure >= 24:
			self.copyIn(self.jourProchain(keepHeureMin = True))
			heure -= 24
		self.heure = (self.heure + heure) % 24

	def addMinute(self,minute):
		"""Ajoute un nombre de minute donnée a une date

		>>> d = Date(2019,5,12,0,5)
		>>> print(d)
		12/5/2019 00:05
		>>> d.addMinute(10)
		>>> print(d)
		12/5/2019 00:15
		"""
		while minute + self.minute >=60:
			self.addHeure(1)
			minute -= 60
		self.minute = (self.minute + minute) % 60

	def removeMinute(self,minute):
		"""Enleve un nombre de minute donnée a une date
	       
		>>> d = Date(2019,3,1,0,5)
		>>> print(d)
		1/3/2019 00:05
		>>> d.removeMinute(6)
		>>> print(d)
		28/2/2019 23:59
		"""
		
		while self.minute - minute < 0:
			self.removeHeure(1)
			minute -= 60
		self.minute = (self.minute - minute) % 60

	def copyIn(self,d):
		"""Copie toute les donnes d'une date donnée dans la date

		>>> d = Date(2019,7,7,12,30)
		>>> print(d)
		7/7/2019 12:30
		>>> d2 = Date(2017,12,6,16,45)
		>>> print(d2)
		6/12/2017 16:45
		>>> d.copyIn(d2)
		>>> print(d)
		6/12/2017 16:45
		>>> d == d2
		True
		>>> d is d2
		False
		"""
		if isinstance(d,self.__class__):
			self.annee = d.annee
			self.mois = d.mois
			self.jour = d.jour
			self.heure = d.heure
			self.minute = d.minute

	def removeHeure(self,heure):
		"""Enleve un nombre d'heure donnée a une date

		>>> d = Date(2019,1,1,0,1)
		>>> print(d)
		1/1/2019 00:01
		>>> d.removeHeure(1)
		>>> print(d)
		31/12/2018 23:01
		"""
		if self.heure - heure < 0:
			self.copyIn(self.jourPrecedent(keepHeureMin = True))
			heure -= 24
		self.heure = (self.heure - heure) % 24

	def addJour(self,jour):
		"""Ajoute un nombre de jour donnée a une date
		
		>>> d = Date(2019,2,25,9,34)
		>>> print(d)
		25/2/2019 09:34
		>>> d.addJour(5)
		>>> print(d)
		2/3/2019 09:34
		"""
		res = self
		for i in range(0,jour,1):
			res = res.jourProchain(keepHeureMin = True)
		self.copyIn(res)

	def removeJour(self,jour):
		"""Enleve un nombre de jour donnée a une date

		>>> d = Date(2019,10,4,5,34)
		>>> print(d)
		4/10/2019 05:34
		>>> d.removeJour(10)
		>>> print(d)
		24/9/2019 05:34
		"""
		res = self
		for i in range(0,jour,1):
			res = res.jourPrecedent(keepHeureMin = True)
		self.copyIn(res)

	estBissextile = property(lambda self : Date.estAnneeBissextile(self.annee))
	estBissextile.__doc__ = "Retourne vrai si la date a une année bissextile, faux sinon"

	def jourPrecedent(self, keepHeureMin = False):
		"""Renvoie le jour précedant un certain jour
		Si keepHeureMin est True, il garde l'heure et la minute de la date donnée, sinon il les definit a 0

		>>> d = Date(2016,3,1,8,9)
		>>> print(d)
		1/3/2016 08:09
		>>> print(d.jourPrecedent(keepHeureMin = True))
		29/2/2016 08:09
		>>> print(d.jourPrecedent(keepHeureMin = False))
		29/2/2016 00:00
		"""
		annee = self.annee
		mois = self.mois
		jour = self.jour
		if keepHeureMin:
			heure = self.heure
			minute = self.minute
		else:
			heure = 0
			minute = 0
		if jour != 1:
			jour -= 1
		elif mois == 1:
			annee -= 1
			mois = 12
			jour = 31
		elif mois in {2,4,6,8,9,11}:
			mois -= 1
			jour = 31
		elif mois in {5,7,10,12}:
			mois -= 1
			jour = 30
		elif self.estBissextile:
			mois = 2
			jour = 29
		else:
			mois = 2
			jour = 28
		return Date(annee,mois,jour,heure,minute)
	
	def jourProchain(self,keepHeureMin = False):
		"""Renvoie le jour suivant un jour donnée.
		Si keepHeureMin est True, il garde l'heure et la minute de la date donnée, sinon il les definit a 0

		>>> d = Date(2019,7,30,5,6)
		>>> print(d)
		30/7/2019 05:06
		>>> print(d.jourProchain(keepHeureMin = False))
		31/7/2019 00:00
		>>> print(d.jourProchain(keepHeureMin = True))
		31/7/2019 05:06
		"""

		annee = self.annee
		mois = self.mois
		jour = self.jour
		if keepHeureMin:
			heure = self.heure
			minute = self.minute
		else:
			heure = 0
			minute = 0
		if mois == 12 and jour == 31:
			annee = annee + 1
			mois = 1
			jour = 1
		elif (mois == 2
		      and ((self.estBissextile and jour == 29)
			   or (not self.estBissextile and jour == 28)
			   or (mois in {1,3,5,7,8,10} and jour == 31)
			   or (mois in {4,6,9,11} and jour == 30))):
			mois += 1
			jour = 1
		else:
			jour += 1
		return Date(annee, mois, jour, heure, minute)
		
	@staticmethod
	def estNbJourValide(annee,mois,jour):
		"""Renvoie true si le numero de jour est logique par rapport au mois et a l'année, non sinon
		
		>>> Date.estNbJourValide(2019,1,0)
		False
		>>> Date.estNbJourValide(2019,4,31)
		False
		>>> Date.estNbJourValide(2019,2,29)
		False
		>>> Date.estNbJourValide(2019,7,31)
		True
		"""
		#Jour pas WTF
		if jour <= 0 or jour > 31:
			return False
		#Mois a 30 jour avec jour n°31
		elif mois in {4,6,9,11} and jour == 31:
			return False
		elif mois == 2:
			if jour > 29:
				return False
			elif (jour == 29
			      and not Date.estAnneeBissextile(annee)):
				return False
			else:
				return True
		else:
			return True
	
	def __repr__(self):
		return str(self.jour)+"/"+str(self.mois)+"/"+str(self.annee)+" "+"{:02d}".format(self.heure)+":"+"{:02d}".format(self.minute)
	def __eq__(self,autre):
		if isinstance(autre,self.__class__):
			return self.annee==autre.annee and self.mois==autre.mois and self.jour==autre.jour and self.heure==autre.heure and self.minute==autre.minute
		return False
	def __ne__(self,autre):
		if isinstance(autre,self.__class__):
			return self.annee!=autre.annee or self.mois!=autre.mois or self.jour!=autre.jour or self.heure!=autre.heure or self.minute!=autre.minute
		return True
	def __lt__(self,autre):
		if self.annee==autre.annee:
			if self.mois==autre.mois:
				if self.jour==autre.jour:
					if self.heure==autre.heure:
						return self.minute<autre.minute
					return self.heure<autre.heure
				return self.jour<autre.jour
			return self.mois<autre.mois
		return self.annee<autre.annee
	def __le__(self,autre):
		if self.annee==autre.annee:
			if self.mois==autre.mois:
				if self.jour==autre.jour:
					if self.heure==autre.heure:
						return self.minute<=autre.minute
					return self.heure<=autre.heure
				return self.jour<=autre.jour
			return self.mois<=autre.mois
		return self.annee<=autre.annee
	def __ge__(self,autre):
		if self.annee==autre.annee:
			if self.mois==autre.mois:
				if self.jour==autre.jour:
					if self.heure==autre.heure:
						return self.minute>=autre.minute
					return self.heure>=autre.heure
				return self.jour>=autre.jour
			return self.mois>=autre.mois
		return self.annee>=autre.annee
	def __gt__(self,autre):
		if self.annee==autre.annee:
			if self.mois==autre.mois:
				if self.jour==autre.jour:
					if self.heure==autre.heure:
						return self.minute>autre.minute
					return self.heure>autre.heure
				return self.jour>autre.jour
			return self.mois>autre.mois
		return self.annee>autre.annee
if __name__ == "__main__":
	import doctest
	doctest.testmod()
